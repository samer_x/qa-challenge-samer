package playstore.gameselection;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GamePo extends GameConstants {
	@FindBy(css = "wishlist-add")
	static WebElement addToWishListbtn;

	@FindBy(linkText = "Remove")
	static WebElement remove;

	@FindBy(linkText = "My wishlist")
	static WebElement myWishList;

	@FindBy(css = "[jscontroller='kSfS7']")
	static WebElement installBttn;

	@FindBy(className = "device-selector-button")
	static WebElement dropDownMenu;

	@FindBy(className = "device-icon")
	static WebElement dropDownMenulist;

	@FindBy(id = "purchase-ok-button")
	static WebElement installpopupbttn;

	@FindBy(id = "close-dialog-button")
	static WebElement okbttn;

	@FindBy(linkText = "Write a Review")
	static WebElement writeReview;

	@FindBy(css = "[maxlength=' 1200 ']")
	static WebElement reviewTxtField;

	@FindBy(className = " fifth-star ")
	static WebElement fifthStar;

	@FindBy(linkText = " SUBMIT ")
	static WebElement submitReview;
}
