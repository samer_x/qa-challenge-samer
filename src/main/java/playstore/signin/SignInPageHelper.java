package playstore.signin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import browser.setup.BrowserSetup;
import playstore.common.CommonHelper;

public class SignInPageHelper extends SignInPagePo {

	WebDriver driver = BrowserSetup.getDriver();

	public SignInPageHelper() {
		PageFactory.initElements(driver, this);
	}

	public void verifyPassword() {
		CommonHelper.clickWhenReady(NxtBttn, 30);
	}

	public void signIn() {
		CommonHelper.clickWhenReady(email, 30);
		email.sendKeys(SignInPageConstants.userName);
		emailverf.click();
		CommonHelper.getWhenVisible(password, 30);
		password.sendKeys(SignInPageConstants.passWord);
		CommonHelper.clickWhenReady(nextbutton, 30);
	}
}
