package playstore.gameselection;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import browser.setup.BrowserSetup;
import playstore.common.CommonHelper;

public class GameHelper extends GamePo {

	WebDriver driver = BrowserSetup.getDriver();

	public GameHelper() {
		PageFactory.initElements(driver, this);
	}

	public void addGameToWishlist() {
		boolean addToWishdisplayed = CommonHelper.isDisplayed(addToWishListbtn);
		if (addToWishdisplayed) {
			CommonHelper.elementClick(addToWishListbtn);
		} else {
			System.out.println("Selected Game Already Added To WishList");
		}
	}

	public void navigatetomywishlist() {
		CommonHelper.clickWhenReady(myWishList, 30);
	}

	public void installGame() {
		boolean installdisplayed = CommonHelper.isDisplayed(installBttn);
		if (installdisplayed) {
			CommonHelper.elementClick(addToWishListbtn);
		} else {
			System.out.println("Selected Game Already Added To WishList");
		}
	}

	public void chooseDevice() throws InterruptedException {
		CommonHelper.sleepForSec(10);
		int numnberOfFrame = driver.findElements(By.tagName("iframe")).size();
		for (int i = 0; i < numnberOfFrame - 1; i++) {
			driver.switchTo().defaultContent();
			driver.switchTo().frame(i);
			if (driver.findElements(By.className("device-selector-button")).size() != 0) {
				dropDownMenu.click();
				dropDownMenulist.click();	
				break;
			}
		}
	}

	public void installGameclick() throws InterruptedException {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(3);
		CommonHelper.elementClick(installpopupbttn);
	}
	public void installGameclickokk() throws InterruptedException {
		int numnberOfFrame = driver.findElements(By.tagName("iframe")).size();
		for (int i = 0; i < numnberOfFrame - 1; i++) {
			driver.switchTo().defaultContent();
			driver.switchTo().frame(i);
			if (driver.findElements(By.id("close-dialog-button")).size() != 0) {
				CommonHelper.elementClick(okbttn);
				CommonHelper.sleepForSec(2);
				driver.switchTo().defaultContent();
				break;
			}
		}
	
	}


	public void writeReview() {
		CommonHelper.clickWhenReady(writeReview, 30);
		CommonHelper.clickWhenReady(reviewTxtField, 30);
		reviewTxtField.sendKeys("Test");
	}

	public void rate5Stars() {
		CommonHelper.elementClick(fifthStar);
		CommonHelper.elementClick(submitReview);
	}

	public void deleteReview() {

	}

}
