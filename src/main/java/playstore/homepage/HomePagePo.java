package playstore.homepage;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePagePo extends HomePageConstants {
	@FindBy(linkText = "Sign in")
	static WebElement sigIn;

	@FindBy(linkText = "Apps")
	static WebElement appsTab;

	@FindBy(linkText = "Games")
	static WebElement games;

	@FindBy(css = ".main-content div:nth-of-type(1) [data-common-cover-type-css]")
	static List<WebElement> allCategories;

	public List<WebElement> getGameList() {
		WebElement firstCategory = allCategories.get(0);
		return firstCategory.findElements(By.className("preview-overlay-container"));
	}
}
