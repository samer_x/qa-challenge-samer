package playstore.test;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import browser.setup.AvailableBrowsers;
import browser.setup.BrowserSetup;
import playstore.common.CommonHelper;
import playstore.gameselection.GameHelper;
import playstore.homepage.HomePageHelper;
import playstore.signin.SignInPageHelper;
import playstore.wishlist.WishlistHelper;

public class PlayStoreTest extends BrowserSetup {

	HomePageHelper homePageHelper;
	GameHelper gameHelper;
	WishlistHelper wishListHelper;
	SignInPageHelper signinPageHelper;

	@BeforeClass
	public void setup() throws Exception {
		BrowserSetup.browserSetup(AvailableBrowsers.CHROME);
		homePageHelper = new HomePageHelper();
		gameHelper = new GameHelper();
		wishListHelper = new WishlistHelper();
		signinPageHelper = new SignInPageHelper();
	}

	@Test
	public void SignIn() {
		homePageHelper.clickSignIn();
		signinPageHelper.signIn();
	}

	@Test(priority = 1, enabled = true, dependsOnMethods = { "SignIn" })
	public void AddingFirstGameToWishlist() {
		homePageHelper.navigateToGamepage();
		homePageHelper.chooseFirstGame();
		gameHelper.addGameToWishlist();
	}

	@Test(priority = 2, enabled = true, dependsOnMethods = { "AddingFirstGameToWishlist" })
	public void AddingLastGameToWishlist() {
		CommonHelper.browserNavigateBack();
		homePageHelper.chooselastGame();
		gameHelper.addGameToWishlist();
	}

	@Test(priority = 3, enabled = true, dependsOnMethods = { "AddingLastGameToWishlist" })
	public void SelectingGameFromWishlist() {
		gameHelper.navigatetomywishlist();
		wishListHelper.selectFirstGame();
	}

	@Test(priority = 4, enabled = true, dependsOnMethods = { "SelectingGameFromWishlist" })
	public void InstallingGame() throws Exception {
		gameHelper.installGame();
		gameHelper.chooseDevice();
		gameHelper.installGameclick();
		driver.switchTo().defaultContent();
	    CommonHelper.sleepForSec(60);
	    CommonHelper.browserRefresh();
	}

	@Test(priority = 5, enabled = true, dependsOnMethods = { "InstallingGame" })
	public void WrittingReview() {
		gameHelper.writeReview();
	}

	@Test(priority = 6, enabled = true, dependsOnMethods = { "WrittingReview" })
	public void RatingFiveStars() {
		gameHelper.rate5Stars();
	}

	@Test(priority = 7, enabled = true, dependsOnMethods = { "WrittingReview" })
	public void deletingReview() {

	}

	@AfterClass
	public void tearDown() throws Exception {
		BrowserSetup.getDriver().quit();
	}
}
