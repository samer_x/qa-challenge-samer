package playstore.signin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPagePo extends SignInPageConstants {

	@FindBy(id = "passwordNext")
	static WebElement NxtBttn;

	@FindBy(id = "identifierId")
	static WebElement email;

	@FindBy(id = "identifierNext")
	static WebElement emailverf;

	@FindBy(css = ".I0VJ4d input")
	static WebElement password;

	@FindBy(id = "passwordNext")
	static WebElement nextbutton;

}
