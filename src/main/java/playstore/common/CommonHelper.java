package playstore.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import browser.setup.BrowserSetup;

public class CommonHelper extends BrowserSetup {

	public static WebElement getWhenVisible(WebElement webElement, int timeout) {
		WebDriver driver = BrowserSetup.getDriver();
		WebElement element = null;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		element = wait.until(ExpectedConditions.visibilityOf(webElement));
		return element;
	}

	public static void clickWhenReady(WebElement games, int timeout) {
		WebElement element = null;
		WebDriver driver = BrowserSetup.getDriver();
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		element = wait.until(ExpectedConditions.visibilityOf(games));
		element.click();
	}

	public static boolean isDisplayed(WebElement element) {
		boolean check = false;
		try {
			check = element.isDisplayed();
			return check;
		} catch (Exception e) {
			return false;
		}
	}

	public static void elementClick(WebElement element) {
		element.click();
	}

	public static void browserNavigateBack() {
		driver.navigate().back();
	}

	public static void browserRefresh() {
		driver.navigate().refresh();
	}
	
	public static void sleepForSec(Integer sec) {
		try {
			Thread.sleep(Integer.valueOf((String.valueOf(sec) + "000")));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}