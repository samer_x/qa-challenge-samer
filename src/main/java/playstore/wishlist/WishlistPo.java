package playstore.wishlist;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class WishlistPo extends WishlistConstants {

	@FindBy(className = "preview-overlay-container")
	static List<WebElement> wishListMenu;
}
