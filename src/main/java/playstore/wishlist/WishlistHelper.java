package playstore.wishlist;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import browser.setup.BrowserSetup;

public class WishlistHelper extends WishlistPo {

	WebDriver driver = BrowserSetup.getDriver();

	public WishlistHelper() {
		PageFactory.initElements(driver, this);
	}

	public void selectFirstGame() {
		wishListMenu.get(0).click();
	}	
}
