package browser.setup;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserSetup {
	public static  WebDriver driver;

	public  static void browserSetup(AvailableBrowsers browser) throws Exception {
		if (browser.equals(AvailableBrowsers.FIREFOX)) {
			System.setProperty("webdriver.firefox.marionette", ".\\geckodriver.exe");
			driver = new FirefoxDriver();
		} else if (browser.equals(AvailableBrowsers.CHROME)) {
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--lang=en-us");
			System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
			driver = new ChromeDriver(options);
		} else {
			throw new Exception("Browser is not correct");
		}
		driver.manage().window().maximize();
		driver.get("https://play.google.com/store?hl=en");
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}

	public static  WebDriver getDriver() {
		return driver;
	}
}
