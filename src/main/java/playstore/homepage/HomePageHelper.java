package playstore.homepage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import browser.setup.BrowserSetup;
import playstore.common.CommonHelper;

public class HomePageHelper extends HomePagePo {

	WebDriver driver = BrowserSetup.getDriver();

	public HomePageHelper() {
		PageFactory.initElements(driver, this);
	}

	public void clickSignIn() {
		CommonHelper.clickWhenReady(sigIn, 30);
	}
	
	public void navigateToGamepage() {
		CommonHelper.clickWhenReady(appsTab, 30);
		CommonHelper.clickWhenReady(games, 30);
	}

	public  void chooseFirstGame() {
		CommonHelper.getWhenVisible(allCategories.get(0), 30);
		WebElement firstGame = getGameList().get(0);			
		CommonHelper.elementClick(firstGame);	
	}
		
	public void chooselastGame() {
		CommonHelper.getWhenVisible(allCategories.get(0), 30);
		WebElement lastGame = getGameList().get(5);			
		CommonHelper.elementClick(lastGame);	
	}
}
